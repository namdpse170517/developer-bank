﻿namespace DeveloperBank
{
    /// <summary>
    /// Class này dùng để người dùng tương tác với dữ liệu và gửi yêu cầu tới BankQueueManager nơi xử lý logic
    /// </summary>
    public class BankUIManager
    {
        public BankQueueManager bankQueueManager;
        public BankUIManager()
        {
            bankQueueManager = new BankQueueManager();
        }

        public void InputCustomerAndAddToTheQueue()
        {
            Console.WriteLine("nhập dữ liệu từ người dùng");
            Console.Write("Tên: ");
            string fullName = Console.ReadLine();
            Console.Write("Số tiền cần rút: ");
            if (!decimal.TryParse(Console.ReadLine(), out decimal withdrawalAmount))
            {
                Console.WriteLine("Giá trị nhập không hợp lệ. Vui lòng nhập số tiền cần rút.");
                return;
            }

            Customer customer = new Customer
            {
                FullName = fullName,
                WithDrawalAmount = withdrawalAmount
            };

            bankQueueManager.AddCustomerToTheQueue(customer);
            Console.WriteLine($"Khách hàng {fullName} đã được thêm vào hàng đợi.");
            Console.WriteLine($"Số thứ tự là: {customer.Number}");
        }

        /// <summary>
        /// 2.Gọi tên khách hàng kế tiếp (VIP)
        /// </summary>
        /// <returns></returns>
        public void DisplayNextVIPCustomer()
        {
            Console.WriteLine("Gọi tên khách hàng kế tiếp (VIP)");
            try
            {
                Customer vipCustomer = bankQueueManager.GetNextVIPCustomer();
                Console.WriteLine($"Số thứ tự: {vipCustomer.Number}");
                Console.WriteLine($"Tên: {vipCustomer.FullName}");
                Console.WriteLine($"Số tiền cần rút: {vipCustomer.WithDrawalAmount}");
                Console.WriteLine($"Đã rút thành công số tiền: {vipCustomer.WithDrawalAmount} VND");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}", ex);
            }
        }

        /// <summary>
        /// 3.Gọi tên khách hàng kế tiếp (Thường).
        /// </summary>
        /// <returns></returns>
        public void DisplayNextEconomyCustomer()
        {

            Console.WriteLine("3.Gọi tên khách hàng kế tiếp (Thường).");
            try
            {
                Customer economy = bankQueueManager.GetNextEconomyCustomer();
                Console.WriteLine($"Số thứ tự: {economy.Number}");
                Console.WriteLine($"Tên: {economy.FullName}");
                Console.WriteLine($"Số tiền cần rút: {economy.WithDrawalAmount}");
                Console.WriteLine($"Đã rút thành công số tiền: {economy.WithDrawalAmount} VND");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}", ex);
            }
        }

        /// <summary>
        /// 4.Khách hàng sắp tới.
        /// </summary>
        public void DisplayNextReadyCustomer()
        {
            Console.WriteLine("4.Khách hàng sắp tới.");
            NextReadyCustomer readyCustomer = bankQueueManager.GetNextReadyCustomer();

            // Hiển thị thông tin về 3 khách hàng gần nhất trong hàng đợi VIP
            Console.WriteLine("Hàng đợi VIP:");
            for (int i = 0; i < readyCustomer.VIPCustomers.Length; i++)
            {
                Customer customer = readyCustomer.VIPCustomers[i];
                if (customer != null)
                {
                    Console.WriteLine($"{i + 1}. {customer.FullName}");
                }
            }

            // Hiển thị thông tin về 3 khách hàng gần nhất trong hàng đợi thường
            Console.WriteLine("Hàng đợi thường:");
            for (int i = 0; i < readyCustomer.EconomyCustomers.Length; i++)
            {
                Customer customer = readyCustomer.EconomyCustomers[i];
                if (customer != null)
                {
                    Console.WriteLine($"{i + 1}. {customer.FullName}");
                }
            }

            // Hiển thị tổng số khách hàng trong từng hàng đợi
            Console.WriteLine($"Tổng số khách VIP đang chờ: {readyCustomer.VIPQueueCount}");
            Console.WriteLine($"Tổng số khách thường đang chờ: {readyCustomer.EconomyQueueCount}");
        }

        /// <summary>
        /// 5.Thống kê
        /// </summary>
        public void DisplayReport()
        {
            Console.WriteLine("5.Thống kê");
            var report = bankQueueManager.CaculationReport();
            Console.WriteLine("Danh sách khách hàng đã rút tiền:");
            for (int i = 0; i < report.CustomerCashOutSuccess.Length; i++)
            {
                Customer customer = report.CustomerCashOutSuccess[i];
                Console.WriteLine($"{i + 1}. {customer.FullName}");
            }
            Console.WriteLine($"Tổng số tiền đã rút: {report.TotalAmountCashOut}");
            Console.WriteLine($"Tổng số tiền còn lại trong hàng đợi: {report.TotalAmountInQueue}");
        }
    }
}
