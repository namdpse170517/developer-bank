﻿using Newtonsoft.Json;
using System;
using System.IO;

public class DataStorageManager
{
    public string vipFilePath ="hellooo";
    public string economyFilePath="abbaababa";
    public void WriteVIPCustomerToFile(Customer vipCustomer)
    {
        try
        {
            using (StreamWriter sw = new StreamWriter(vipFilePath, true))
            {
                string jsonData = JsonConvert.SerializeObject(vipCustomer);
                sw.WriteLine(jsonData);
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Lỗi khi ghi khách hàng VIP vào file: {ex.Message}");
        }
    }
    public void WriteEconomyCustomerToFile(Customer economyCustomer)
    {
        try
        {
            using (StreamWriter sw = new StreamWriter(economyFilePath, true))
            {
                string jsonData = JsonConvert.SerializeObject(economyCustomer);
                sw.WriteLine(jsonData);
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Lỗi khi ghi khách hàng thường vào file: {ex.Message}");
        }
    }

    public Customer[] ReadVIPCustomersFromFile()
    {
        if (!File.Exists(vipFilePath))
        {
            return new Customer[0]; 
        }

        try
        {
            string[] lines = File.ReadAllLines(vipFilePath);
            Customer[] vipCustomers = new Customer[lines.Length];
            for (int i = 0; i < lines.Length; i++)
            {
                vipCustomers[i] = JsonConvert.DeserializeObject<Customer>(lines[i]);
            }
            return vipCustomers;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Lỗi khi đọc khách hàng VIP từ file: {ex.Message}");
            return new Customer[0]; 
        }
    }
    public Customer[] ReadEconomyCustomersFromFile()
    {
        if (!File.Exists(economyFilePath))
        {
            return new Customer[0]; 
        }

        try
        {
            string[] lines = File.ReadAllLines(economyFilePath);
            Customer[] economyCustomers = new Customer[lines.Length];
            for (int i = 0; i < lines.Length; i++)
            {
                economyCustomers[i] = JsonConvert.DeserializeObject<Customer>(lines[i]);
            }
            return economyCustomers;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Lỗi khi đọc khách hàng thường từ file: {ex.Message}");
            return new Customer[0]; 
        }
    }
}
