﻿/// <summary>
/// Sử lý logic của chương trình
/// </summary>
public class BankQueueManager
{
    /// <summary>
    /// Hằng đợi khách hàng thường
    /// </summary>
    public Queue EconomyQueue { get; set; }

    /// <summary>
    /// Hằng đợi của khách hàng VIP
    /// </summary>
    public Queue VIPQueue { get; set; }

    /// <summary>
    /// Danh sách khách hàng rút tiền thành công
    /// </summary>
    public LinkedList CustomerSuccessCashOut { get; set; }
    public int currentVIPNumber = 0;
    public int currentEconomyNumber = 0;
    public int VIPQueueCount => VIPQueue.Count;
    public int EconomyQueueCount => EconomyQueue.Count;

    public BankQueueManager()
    {
        EconomyQueue = new Queue();
        VIPQueue = new Queue();
        CustomerSuccessCashOut = new LinkedList();
    }

    /// <summary>
    /// 1.Thêm khách hàng mới vào hàng đợi.
    /// </summary>
    /// <param name="customer"></param>
    public void AddCustomerToTheQueue(Customer customer)
    {
        if (customer.WithDrawalAmount > 500)
        {
            VIPQueue.Enqueue(customer);
            currentVIPNumber++;
            customer.Number = currentVIPNumber;
        }
        else
        {
            EconomyQueue.Enqueue(customer);
            currentEconomyNumber++;
            customer.Number = currentEconomyNumber;
        }
    }

    /// <summary>
    /// 2.Gọi tên khách hàng kế tiếp (VIP)
    /// </summary>
    /// <returns></returns>
    public Customer GetNextVIPCustomer()
    {
        Customer vipCustomer = VIPQueue.Dequeue();
        CustomerSuccessCashOut.InsertNodeToLast(new Node { DataOfNode = vipCustomer });
        return vipCustomer;
    }

    /// <summary>
    /// 3.Gọi tên khách hàng kế tiếp (Thường).
    /// </summary>
    /// <returns></returns>
    public Customer GetNextEconomyCustomer()
    {
        Customer economyCustomer = EconomyQueue.Dequeue();
        CustomerSuccessCashOut.InsertNodeToLast(new Node { DataOfNode = economyCustomer });
        return economyCustomer;
    }

    /// <summary>
    /// 4. Lấy ra danh sách Khách hàng sắp tới.
    /// </summary>
    public NextReadyCustomer GetNextReadyCustomer()
    {
        NextReadyCustomer nextReadyCustomer = new NextReadyCustomer();

        Customer[] vipCustomers = VIPQueue.GetFirst3Node();
        nextReadyCustomer.VIPCustomers = vipCustomers;

        Customer[] economyCustomers = EconomyQueue.GetFirst3Node();
        nextReadyCustomer.EconomyCustomers = economyCustomers;

        nextReadyCustomer.VIPQueueCount = VIPQueue.Count;
        nextReadyCustomer.EconomyQueueCount = EconomyQueue.Count;

        return nextReadyCustomer;
    }

    /// <summary>
    /// 5.Thống kê về hệ thống
    /// </summary>
    public ReportBank CaculationReport()
    {
        ReportBank report = new ReportBank();
        int customerCount = CustomerSuccessCashOut.Count();
        report.CustomerCashOutSuccess = new Customer[customerCount];
        Node current = CustomerSuccessCashOut.Head;
        int index = 0;
        while (current != null)
        {
            report.CustomerCashOutSuccess[index] = current.DataOfNode;
            current = current.NextNode;
            index++;
        }
        report.TotalAmountCashOut = CustomerSuccessCashOut.TotalAmount();
        report.TotalAmountInQueue = EconomyQueue.CalculatedMoneyInQueue() + VIPQueue.CalculatedMoneyInQueue();

        return report;
    }
}

