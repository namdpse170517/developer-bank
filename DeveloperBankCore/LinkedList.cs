﻿/// <summary>
/// Cấu trúc chứa dữ liệu thực sự
/// </summary>
public class LinkedList
{
    public Node Head { get; set; }

    public void InsertNodeToLast(Node node)
    {
        if (Head is null)
        {
            Head = node;
        }
        else
        {
            Node current = Head;
            while (current.NextNode is not null)
            {
                current = current.NextNode;
            }
            current.NextNode = node;
        }
    }

    public Node GetFirstNodeAndRemove()
    {
        if (Head is null)
        {
            throw new Exception("Không có khách hàng nào !!!");
        }
        Node firstNode = Head;
        Head = Head.NextNode;
        return firstNode;
    }

    public Node[] GetFirstThreeNode()
    {
        Node[] firstThreeNodes = new Node[3];
        Node current = Head;
        int count = 0;
        while (current is not null && count < 3)
        {
            firstThreeNodes[count] = current;
            current = current.NextNode;
            count++;
        }
        return firstThreeNodes;
    }

    public int Count()
    {
        int count = 0;
        Node current = Head;
        while (current is not null)
        {
            count++;
            current = current.NextNode;
        }
        return count;
    }

    public decimal TotalAmount()
    {
        decimal totalAmount = 0;
        Node current = Head;
        while (current is not null)
        {
            totalAmount = totalAmount + current.DataOfNode.WithDrawalAmount;
            current = current.NextNode;
        }
        return totalAmount;
    }
}

public class Node
{
    public Customer DataOfNode { get; set; }
    public Node NextNode { get; set; }
}